import os
from flattener import flatten_image
from color_manager import convert_colors_to_rgb

def main():
    input_folder = 'input_images'
    output_folder = 'output_images'
    colors = convert_colors_to_rgb("palettes/default.txt")

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for file_name in os.listdir(input_folder):
        input_path = os.path.join(input_folder, file_name)
        output_file_name = os.path.splitext(file_name)[0]
        output_path = os.path.join(output_folder, output_file_name)
        
        flatten_image(input_path, output_path, colors)
        print(f"Processed: {file_name}")


if __name__ == "__main__":
    main()