# color-flattener
LOL

## installation
Obligatory make sure you have python3 installed, open a terminal and check. Then cd into wherever the code is cloned to do the installation and running. I'm assuming you know basic terminal stuff like `git clone`, `cd`, and `pip install` kind of stuff. 

We use pipenv. If you don't have it, install it using `pip install pipenv`. Then, run `pipenv install`. This will install the packages we use for image processing and stuff.

## usage
This image color flattener will process all the images that you put in the "input_images" folder, and output them in an "output_images" folder.

Aside from putting the images into the input folder, you also need to fill the `palettes/default.txt` folder with all the colors. They should be in hex format, separated by new lines. Don't add anything else to the file. There should be at least one color, just follow the example.

Now to run the process, you can copy the command in the makefile, which should look like `pipenv run python main.py`. You can't just run `python main.py` because you might get different results. Pipenv makes sure we get the same output. Anyway, after a few seconds it should finish processing the first image, and continue for the rest of them.

There will be two outputs, one being an exact pixelated version, and the other will be very lightly smoothed.

