from PIL import Image, ImageFilter
import colorsys

def flatten_image(image_path, output_path, colors):
    image = Image.open(image_path)
    width, height = image.size

    flattened_image = Image.new("RGB", (width, height))
    for x in range(width):
        for y in range(height):
            pixel = image.getpixel((x, y))
            flattened_image.putpixel((x, y), flatten_simple(colors, pixel))

    smoothed_image = flattened_image.filter(ImageFilter.GaussianBlur(radius=1))

    flattened_image.save(output_path + ".png")
    smoothed_image.save(output_path + "_smooth.png")


def flatten_simple(colors, pixel_rgb):
    closest_color = None
    min_distance = float('inf')

    for color in colors:
        distance = color_distance(color, pixel_rgb)
        
        if distance < min_distance:
            min_distance = distance
            closest_color = color

    return closest_color

def flatten_huematch(colors, pixel_rgb):
    closest_color = min(colors, key=lambda color: color_distance(color, pixel_rgb))
    closest_hue = colorsys.rgb_to_hsv(*closest_color)[0]
    new_hsv = (closest_hue, 1, 1)
    new_rgb = colorsys.hsv_to_rgb(*new_hsv)
    return tuple(int(value * 255) for value in new_rgb)

def color_distance(color1, color2):
    r_diff = color1[0] - color2[0]
    g_diff = color1[1] - color2[1]
    b_diff = color1[2] - color2[2]
    return (r_diff**2 + g_diff**2 + b_diff**2) ** 0.5