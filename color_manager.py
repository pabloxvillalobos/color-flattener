def convert_colors_to_rgb(file_path):
    colors_rgb = []
    
    with open(file_path, 'r') as file:
        lines = file.readlines()

        for line in lines:
            # Remove leading/trailing whitespaces and newline characters
            color_hex = line.strip()
            
            # Convert hex color to RGB values
            r = int(color_hex[1:3], 16)
            g = int(color_hex[3:5], 16)
            b = int(color_hex[5:7], 16)
            
            # Append RGB values as a tuple to the list
            colors_rgb.append((r, g, b))

    return colors_rgb